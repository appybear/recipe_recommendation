<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RecommendTest extends TestCase
{
    public function testRecommendation()
    {
        $this->json('POST', '/recommend', ['csv' => 'fridge.csv', 'json' => 'json.txt'])
            ->seeJson([
                'recommendation' => 'grilledcheeseontoast',
            ]);
    }

    public function testRecommendation2()
    {
        $this->json('POST', '/recommend', ['csv' => 'fridge2.csv', 'json' => 'json2.txt'])
            ->seeJson([
                'recommendation' => 'grilledcheeseontoast',
            ]);
    }

    public function testTakeOut()
    {
        $this->json('POST', '/recommend', ['json' => 'json.txt'])
            ->seeJson([
                'recommendation' => 'Order Takeout',
            ]);
    }
}
