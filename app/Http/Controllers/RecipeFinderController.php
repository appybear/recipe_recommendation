<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class RecipeFinderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Read the CSV from location based on filename sent in
     * @param $filename
     * @return array
     */
    private function readCSV($filename){
        $file = fopen($filename, 'r');
        $result=[];
        $count = 0;
        while (($line = fgetcsv($file)) !== FALSE) {
            //$line is an array of the csv elements
            $result[] = $line;
            $count++;
        }
        fclose($file);
        return $result;
    }

    /**
     * Convert date to shorten a bit of code and just in case you have different date formats
     * @param $format
     * @param $date
     * @return string
     */
    private function ConvertDate($format, $date)
    {
        //using the Carbon library to format dates
        return Carbon::createFromFormat($format,$date)->toDateTimeString();
    }

    /**
     * Clean the fridge of expired items
     * @param $items
     * @return mixed
     */
    public function cleanFridge($items){
        //if there are items to check
        if (count($items) != 0){
            //check if there are expired items
            foreach($items as $index=>$item){
                $item[3] = $this->ConvertDate('d/m/Y', $item[3]);//convert date
                if ( $item[3] < date('Y-m-d')){
                    unset($items[$index]);//remove it from the list
                }
            }
        }
        //return all items which aren't expired
        return ($items);
    }

    /**
     * Check which recipes can be made with the ingredients we have
     * @param $recipes
     * @param $items
     * @return array
     */
    public function makeableRecipes($recipes, $items){
        //initialize variables
        $makeable = [];
        $dates = [];
        //loop through recipes
        foreach($recipes as $i=>$recipe){
            //check the ingredients against the items
            foreach($recipe['ingredients'] as $j=>$ingredient){
                foreach ($items as $k=>$item){
                    //if the item is in the pantry, check the item off the recipe list
                    //and keep the dates of the items for checking later if needed
                    if (strtoupper($ingredient['item']) == strtoupper($item[0]) && $ingredient['amount'] < $item[1]){
                        $dates[] = $item[3];
                        unset($recipe['ingredients'][$j]);//check the item off the list
                    }
                }
            }               //all items checked off
            if (count($recipe['ingredients'])==0){
                $makeable[] = array($recipe['name'], $dates);
            }
            $dates=[];//empty dates
        }
        //return the makeable recipes
        return $makeable;
    }

    /**
     * Make the final choice
     * @param $makeable
     * @return string
     */
    private function makeChoice($makeable){
        //if there was nothing to make
        if (count($makeable) == 0){
            $choice = 'Order Takeout';
        }
        //if there is only one thing to make
        else if (count($makeable) == 1){
            $choice = $makeable[0][0];//first element name
        }
        else {
            //initialise variables
            $choice="";
            $dates="";
            //loop through what we can make and look for the recipe with the item closest to expiry
            foreach ($makeable as $index=>$recipe){
                foreach($recipe[1] as $date){
                    if ($choice == ""){
                        $dates = $date;
                        $choice = $recipe[0];
                    }
                    else if ($dates > $date){
                        $choice = $recipe[0];
                        $dates = $date;
                    }
                }
            }
        }
        return $choice;
    }

    /**
     * The Main logic method
     * @param Request $request
     * @param $csv
     * @param $json
     * @return \Illuminate\Http\JsonResponse
     */
    public function recommendRecipe(Request $request){

        $csv = $request->input('csv');
        $json = $request->input('json');
        //logic for recommendation
        //clean the fridge of expired items
        $items = $this->cleanFridge($this->readCSV($csv));
        //json decode the recipes in json format
        $recipes = json_decode(file_get_contents($json), true);
        //check what recipes can be made
        $makeable = $this->makeableRecipes($recipes, $items);
        //of the recipes returned, make the one with the closest expiry date
        $return["recommendation"] = $this->makeChoice($makeable);
        //return json response
        return response()->json($return);
    }
}
